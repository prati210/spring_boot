package com.Hello.HelloWorld.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hello")
public class HelloController
{
    @GetMapping
    public String hello(){
        return "Hello Pratikshya";
    }

    @GetMapping("/name")
    public String getRequestParam(@RequestParam String name) {
        return "My Name is :"+name;
    }

    @PostMapping("/empty")
    public String getRequestParam() {
        return "Twinkle";
    }
}
